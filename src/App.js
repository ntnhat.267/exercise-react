import React, { useEffect } from "react";
import "./App.css";
import Todo from "./component/Todo";
import TodoForm from "./component/TodoForm";
import { useFetchData } from "./hooks/useFetchData";
import { deleteData, postData, updateData } from "./service";

function App() {
  const { data } = useFetchData("todos");
  const [todos, setTodos] = React.useState([]);

  useEffect(() => {
    setTodos(data);
  }, [data]);

  const addTodo = async (text) => {
    await postData("todos", text);
    const newTodos = [...todos, { text }];
    setTodos(newTodos);
  };

  const completeTodo = async (id) => {
    await updateData(`todos/${id}`);
    const newTodos = [...todos];
    setTodos(
      newTodos.map((todo) => {
        if (todo.id.toString() === id.toString()) {
          return {
            ...todo,
            isCompleted: true,
          };
        }
        return todo;
      })
    );
  };

  const removeTodo = async (id) => {
    await deleteData(`todos/${id}`);
    const newTodos = [...todos];
    setTodos(newTodos.filter((todo) => todo.id.toString() !== id.toString()));
  };

  return (
    <div className="app">
      <div className="todo-list">
        {todos.map((todo, index) => (
          <Todo
            key={index}
            index={index}
            todo={todo}
            completeTodo={completeTodo}
            removeTodo={removeTodo}
          />
        ))}
        <TodoForm addTodo={addTodo} />
      </div>
    </div>
  );
}

export default App;
