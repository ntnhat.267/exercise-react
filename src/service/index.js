export const postData = async (endpoint, data) => {
  try {
    console.log(1123, data);
    await fetch(`http://localhost:5000/api/${endpoint}`, {
      method: "POST",
      body: JSON.stringify(data)
    });
  } catch (error) {
    console.log("🚀 ~ postData ~ error:", error);
  }
};

export const updateData   = async (endpoint, data) => {
    try {
      console.log(1123, data);
      await fetch(`http://localhost:5000/api/${endpoint}`, {
        method: "PUT",
        body: JSON.stringify(data)
      });
    } catch (error) {
      console.log("🚀 ~ postData ~ error:", error);
    }
  };

  export const deleteData   = async (endpoint, data) => {
    try {
      console.log(1123, data);
      await fetch(`http://localhost:5000/api/${endpoint}`, {
        method: "DELETE",
      });
    } catch (error) {
      console.log("🚀 ~ postData ~ error:", error);
    }
  };
