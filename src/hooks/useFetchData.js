import { useEffect, useState } from "react";

export const useFetchData = (endpoint) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchData = async () => {
    try {
      setLoading(true);
      const resp = await fetch(`http://localhost:5000/api/${endpoint}`);
      const resspData = await resp.json();
      console.log("🚀 ~ fetchData ~ resspData:", resspData);
      setData(resspData.data);
    } catch (error) {
      console.log("🚀 ~ fetchData ~ error:", error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchData()
  }, [])
  return {data, loading}
};
